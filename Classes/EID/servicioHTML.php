<?php
/**
 * Created by PhpStorm.
 * User: Francisco Javier
 * Date: 24-08-2016
 * Time: 9:41
 */

use Dompdf\Dompdf;
include 'Fpdf/fpdf.php';
include 'Fpdi/fpdi.php';

if (!defined ('PATH_typo3conf')) die ('Access denied.');

\TYPO3\CMS\Frontend\Utility\EidUtility::initTCA();

$id = isset($HTTP_GET_VARS['id'])?$HTTP_GET_VARS['id']:0;
header('Content-Type: application/json');

$TSFE = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $GLOBALS['TYPO3_CONF_VARS'], $id, '0', 1);
$GLOBALS['TSFE'] = $TSFE;
$GLOBALS['TSFE']->initFEuser(); // Get FE User Information
$GLOBALS['TSFE']->fetch_the_id();
$GLOBALS['TSFE']->getPageAndRootline();
$GLOBALS['TSFE']->initTemplate();
$GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
$GLOBALS['TSFE']->forceTemplateParsing = 1;
$GLOBALS['TSFE']->getConfigArray();
$GLOBALS['TSFE']->register['hello'] = 1;


/** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');

$rutaInstalacion = PATH_site;
$directorio = str_replace("/var/www/html/","",$rutaInstalacion);

$jsonMarcas = "";

$jsonMarcas = $_POST["json"];

$configuracionPDF = json_decode($jsonMarcas);

//Se obtiene el nombre del servidor y la fecha actual para agregarla al archivo PDF generado
$urlServidor = "http://".$_SERVER["SERVER_NAME"]."/";
$fechaArchivo = date("d-m-Y_H-i-s");
$nombreArchivoFinal = $configuracionPDF->nombreArchivo;


if (!isset($configuracionPDF->nombreArchivo) || $configuracionPDF->nombreArchivo == ''){
    error_log("Falta nombre para al archivo de salida");
    throw new Exception('Falta nombre para el archivo de salida');
}

//Se accede al archivo txt que contiene el codigo HTML usado como plantilla (r para abrirlo solo como lectura)
$archivo = fopen(PATH_site .$configuracionPDF->ubicacionPlantilla,"r");

//Se lee el contenido del archivo y se guarda en una variable
$texto = fread($archivo, filesize(PATH_site .$configuracionPDF->ubicacionPlantilla));

if (empty($configuracionPDF->portadaDocumento) || (isset($configuracionPDF->portadaDocumento)==false)) {

    $options = new \Dompdf\Options();

    $options->set('isRemoteEnabled', true);

    $dompdf = new Dompdf($options);

    //Se accede al archivo txt que contiene el codigo HTML usado como plantilla (r para abrirlo solo como lectura)
    $archivo = fopen(PATH_site .$configuracionPDF->ubicacionPlantilla,"r");

//Se lee el contenido del archivo y se guarda en una variable
    $texto = fread($archivo, filesize(PATH_site .$configuracionPDF->ubicacionPlantilla));

    //Se recorre el arreglo que contiene cada una de las marcas y se hace el reemplazo usando las marcas del html
    foreach ($configuracionPDF->marcadoPlantilla as $codigo => $valor) {
        if ($codigo[0] != "@") {

            //$valor = str_replace(" ","&#160;",$valor);

            $valor = str_replace("\t","&#8212; ",$valor);
            //$valor = str_replace("\t","&#160;&#160;&#160;&#160;",$valor);

            $valor = nl2br($valor);
        }

        $texto = str_replace($codigo, $valor, $texto);
        //file_put_contents(PATH_site."fileadmin/Plantillas/htmlImagenesCont".$fechaArchivo."html",$texto);
    }

    //Luego de haber usado lap lantilla txt, el archivo se cierra
    fclose($archivo);

    //Configuracion del archivo PDF y carga del contenido HTML con las marcas reemplazadas
    $dompdf->setPaper("Letter", "Portrait");
    $dompdf->loadHtml($texto);
    $dompdf ->render();

    if (!is_dir(PATH_site.$configuracionPDF->rutaSalida)) {
        mkdir(PATH_site.$configuracionPDF->rutaSalida, 0777, true);
    }

    //Se guarda el archivo PDF con el nombre y ruta especificada en el servicio
    $directorio_guardado = PATH_site .$configuracionPDF->rutaSalida.$nombreArchivoFinal.".pdf";

    if (file_exists($directorio_guardado)) {
        unlink($directorio_guardado);
        file_put_contents(PATH_site .$configuracionPDF->rutaSalida.$nombreArchivoFinal.".pdf", $dompdf->output());
    } else {
        file_put_contents(PATH_site .$configuracionPDF->rutaSalida.$nombreArchivoFinal.".pdf", $dompdf->output());
    }

    //Se genera el enlace de descarga que se envia a la vista
    $ruta_pdf = $urlServidor.$directorio.$configuracionPDF->rutaSalida.$nombreArchivoFinal.".pdf";
    $enlace_pdf = $ruta_pdf;

    echo $enlace_pdf;
} else {
    $dompdf = new Dompdf();
    $dompdfContent = new Dompdf();

    $archivoPortada = fopen(PATH_site .$configuracionPDF->portadaDocumento->ubicacionPlantilla,"r");
    $archivoContenido = fopen(PATH_site .$configuracionPDF->ubicacionPlantilla,"r");

    $portada = fread($archivoPortada, filesize(PATH_site .$configuracionPDF->portadaDocumento->ubicacionPlantilla));
    $contenido = fread($archivoContenido, filesize(PATH_site .$configuracionPDF->ubicacionPlantilla));

    //Portada
    //Se recorre el arreglo que contiene cada una de las marcas y se hace el reemplazo usando las marcas del html
    foreach ($configuracionPDF->portadaDocumento->marcadoPlantilla as $codigo => $valor) {
        if ($codigo[0] != "@") {

            $valor = str_replace("\t","&#8212; ",$valor);
            //$valor = str_replace("\t","&#160;&#160;&#160;&#160;",$valor);

            $valor = nl2br($valor);
        }

        $portada = str_replace($codigo, $valor, $portada);
    }

    //Luego de haber usado lap lantilla txt, el archivo se cierra
    fclose($archivoPortada);

    //Configuracion del archivo PDF y carga del contenido HTML con las marcas reemplazadas
    $dompdf->setPaper("Letter", "Portrait");
    $dompdf->loadHtml($portada);
    $dompdf ->render();

    if (!is_dir(PATH_site.$configuracionPDF->portadaDocumento->salidaDocumentoTemporal)) {
        mkdir(PATH_site.$configuracionPDF->portadaDocumento->salidaDocumentoTemporal, 0777, true);
    }

    //Se guarda la portada con el nombre y ruta especificada en el servicio
    $archivo_guardado_portada = PATH_site .$configuracionPDF->portadaDocumento->salidaDocumentoTemporal."Portada".$fechaArchivo.".pdf";
    file_put_contents($archivo_guardado_portada, $dompdf->output());
    //Fin Portada

    //Contenido
    //Se recorre el arreglo que contiene cada una de las marcas y se hace el reemplazo usando las marcas del html
    foreach ($configuracionPDF->marcadoPlantilla as $codigo => $valor) {
        if ($codigo[0] != "@") {

            //$valor = str_replace(" ","&#160;",$valor);

            $valor = str_replace("\t","&#8212; ",$valor);
            //$valor = str_replace("\t","&#160;&#160;&#160;&#160;",$valor);

            $valor = nl2br($valor);
        }

        $contenido = str_replace($codigo, $valor, $contenido);
        //file_put_contents(PATH_site."fileadmin/Plantillas/htmlImagenesCont".$fechaArchivo."html",$texto);
    }
    //Luego de haber usado lap lantilla txt, el archivo se cierra
    fclose($archivoContenido);

    //Configuracion del archivo PDF y carga del contenido HTML con las marcas reemplazadas
    $dompdfContent->setPaper("Letter", "Portrait");
    $dompdfContent->loadHtml($contenido);
    $dompdfContent ->render();

    if (!is_dir(PATH_site.$configuracionPDF->portadaDocumento->salidaDocumentoTemporal)) {
        mkdir(PATH_site.$configuracionPDF->portadaDocumento->salidaDocumentoTemporal, 0777, true);
    }

    //Se guarda la parte de contenido con el nombre y ruta especificada en el servicio
    $archivo_guardado_contenido = PATH_site .$configuracionPDF->portadaDocumento->salidaDocumentoTemporal."Contenido".$fechaArchivo.".pdf";
    file_put_contents($archivo_guardado_contenido, $dompdfContent->output());
    //Fin Contenido

    //Unir PDFs
    // define some files to concatenate
    $files = array(
        $archivo_guardado_portada,
        $archivo_guardado_contenido
    );

    // initiate FPDI
        $pdfUnion = new FPDI();

    // iterate through the files
        foreach ($files AS $file) {
            // get the page count
            $pageCount = $pdfUnion->setSourceFile($file);
            // iterate through all pages
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                // import a page
                $templateId = $pdfUnion->importPage($pageNo);
                // get the size of the imported page
                $size = $pdfUnion->getTemplateSize($templateId);

                // create a page (landscape or portrait depending on the imported page size)
                if ($size['w'] > $size['h']) {
                    $pdfUnion->AddPage('L', array($size['w'], $size['h']));
                } else {
                    $pdfUnion->AddPage('P', array($size['w'], $size['h']));
                }

                // use the imported page
                $pdfUnion->useTemplate($templateId);
            }
        }

    if (!is_dir(PATH_site.$configuracionPDF->rutaSalida)) {
        mkdir(PATH_site.$configuracionPDF->rutaSalida, 0777, true);
    }

    if (file_exists(PATH_site .$configuracionPDF->rutaSalida.$nombreArchivoFinal.".pdf")) {
        unlink(PATH_site .$configuracionPDF->rutaSalida.$nombreArchivoFinal.".pdf");
        $pdfUnion->Output(PATH_site .$configuracionPDF->rutaSalida.$nombreArchivoFinal.".pdf",'F');
    } else {
        $pdfUnion->Output(PATH_site .$configuracionPDF->rutaSalida.$nombreArchivoFinal.".pdf",'F');
    }


    //Borrar archivos
    unlink($archivo_guardado_portada);
    unlink($archivo_guardado_contenido);

    //Se genera el enlace de descarga que se envia a la vista
    $ruta_pdf = $urlServidor.$directorio.$configuracionPDF->rutaSalida.$nombreArchivoFinal.".pdf";
    $enlace_pdf = $ruta_pdf;

    echo $enlace_pdf;
}