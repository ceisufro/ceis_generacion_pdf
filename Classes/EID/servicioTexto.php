<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 24-08-2016
 * Time: 9:41
 */

if (!defined ('PATH_typo3conf')) die ('Access denied.');

\TYPO3\CMS\Frontend\Utility\EidUtility::initTCA();

$id = isset($HTTP_GET_VARS['id'])?$HTTP_GET_VARS['id']:0;
header('Content-Type: application/json');

$TSFE = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $GLOBALS['TYPO3_CONF_VARS'], $id, '0', 1);
$GLOBALS['TSFE'] = $TSFE;
$GLOBALS['TSFE']->initFEuser(); // Get FE User Information
$GLOBALS['TSFE']->fetch_the_id();
$GLOBALS['TSFE']->getPageAndRootline();
$GLOBALS['TSFE']->initTemplate();
$GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
$GLOBALS['TSFE']->forceTemplateParsing = 1;
$GLOBALS['TSFE']->getConfigArray();
$GLOBALS['TSFE']->register['hello'] = 1;

/** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');

$textos = array(
    "nombreArchivo" => "PDF_texto",
    "ubicacionPDFeditable" => "fileadmin/Archivos_PDF/PDF_editable2pag.pdf",
    "rutaSalida" => "fileadmin/Archivos_PDF/",
    "parametrosTextos" => array(
        array("numPagina" => 1,
            "texto" => "Texto 1",
            "color" => "20,30,20",
            "fuente" => "Courier",
            "estilo" => "I",
            "tamano" => 22,
            "posicionX" => 20,
            "posicionY" => 20),
        array("numPagina" => 2,
            "texto" => "Texto 2",
            "color" => "20,30,20",
            "fuente" => "Helvetica",
            "estilo" => "B",
            "tamano" => 8,
            "posicionX" => 20,
            "posicionY" => 20),
        array("numPagina" => 1,
            "texto" => "Texto 3",
            "color" => "20,30,20",
            "fuente" => "Times",
            "estilo" => "U",
            "tamano" => 18,
            "posicionX" => 35,
            "posicionY" => 100)
    )
);

echo json_encode($textos);