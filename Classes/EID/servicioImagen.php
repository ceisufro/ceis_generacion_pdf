<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 24-08-2016
 * Time: 9:41
 */

if (!defined ('PATH_typo3conf')) die ('Access denied.');

\TYPO3\CMS\Frontend\Utility\EidUtility::initTCA();

$id = isset($HTTP_GET_VARS['id'])?$HTTP_GET_VARS['id']:0;
header('Content-Type: application/json');

$TSFE = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $GLOBALS['TYPO3_CONF_VARS'], $id, '0', 1);
$GLOBALS['TSFE'] = $TSFE;
$GLOBALS['TSFE']->initFEuser(); // Get FE User Information
$GLOBALS['TSFE']->fetch_the_id();
$GLOBALS['TSFE']->getPageAndRootline();
$GLOBALS['TSFE']->initTemplate();
$GLOBALS['TSFE']->tmpl->getFileName_backPath = PATH_site;
$GLOBALS['TSFE']->forceTemplateParsing = 1;
$GLOBALS['TSFE']->getConfigArray();
$GLOBALS['TSFE']->register['hello'] = 1;

/** @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager */
$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');

$jsonMarcas = $_POST["json"];

//Se obtiene el arreglo con la configuracion de edicion
$configuracionPDF = json_decode($jsonMarcas);

//Se obtiene el nombre del servidor y la fecha actual para agregarla al archivo PDF generado
$urlServidor = "http://".$_SERVER["SERVER_NAME"]."/";
//Fecha para asignar a archivo
$fechaArchivo = date("d-m-Y_H-i-s");
//Extraccion nombre del archivos del arreglo
if (!isset($configuracionPDF->nombreArchivo) || $configuracionPDF->nombreArchivo == ''){
    error_log("Falta nombre para al archivo de salida");
    throw new Exception('Falta nombre para el archivo de salida');
}
$nombreArchivoFinal = $configuracionPDF->nombreArchivo;

//Se lee el contenido del archivo y se guarda en una variable
$texto = fread($archivo, filesize(PATH_site .$configuracionPDF->ubicacionPlantilla));

$imagenes = array(
    "nombreArchivo" => "PDF_imagen",
    "ubicacionPDFeditable" => "fileadmin/Archivos_PDF/PDF_editable2pag.pdf",
    "rutaSalida" => "fileadmin/Archivos_PDF/",
    "parametrosImagenes" => array(
        array("numPagina" => 1,
            "urlImagen" => "http://www.tnrelaciones.com/informacion/wp-content/uploads/2014/01/la-vida63.jpg",
            "imagenEjeX" => 30,
            "imagenEjeY" => 40,
            "anchoImagen" => 100,
            "altoImagen" => 50),
        array("numPagina" => 2,
            "urlImagen" => "https://1.bp.blogspot.com/-2lQ6gAmSEpU/VvFudilUuKI/AAAAAAAQKck/U75eVkak_VwnkIOfPcq5PZLNFm-_x3CnA/s1600/humor-adictamente%2B%252835%2529.jpg",
            "imagenEjeX" => 25,
            "imagenEjeY" => 25,
            "anchoImagen" => 80,
            "altoImagen" => 60),
        array("numPagina" => 2,
            "urlImagen" => "http://pelotaenlared.cl/wp-content/uploads/2016/08/14064042_1265731166800823_1203648875309617275_n-750x400.jpg",
            "imagenEjeX" => 30,
            "imagenEjeY" => 150,
            "anchoImagen" => 70,
            "altoImagen" => 40)
    )
);
echo json_encode($imagenes);