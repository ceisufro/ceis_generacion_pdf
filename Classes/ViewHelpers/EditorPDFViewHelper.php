<?php

/**
 * Created by PhpStorm.
 * User: francisco
 * Date: 12/08/16
 * Time: 16:35
 */

namespace Francisco\EditorPDF\ViewHelpers;

use TYPO3\CMS\Fluid\ViewHelpers\Form\AbstractFormViewHelper;
use TYPO3\CMS\Fluid\View\StandaloneView;

header("Content-Type: text/html;charset=utf-8");

class EditorPDFViewHelper extends AbstractFormViewHelper
{
    /**
     * @param string $servicioHTML
     * @param string $servicioImagen
     * @param string $servicioTexto
     * @param string $textoHTML
     * @param string $nombreArchivo
     * @param string $rutaSalida
     * @return mixed
     */
    public function render($servicioHTML,$servicioImagen,$servicioTexto,$textoHTML,$nombreArchivo,$rutaSalida) {

        error_log("Viewhelper no hace nada");

        return "Hola Mundo";

        //Obtiene directorio adicional para agregarlo a la direccion de descarga

        /**$rutaInstalacion = PATH_site;
        $directorio = str_replace("/var/www/html/","",$rutaInstalacion);

        $directorioVista = PATH_site."typo3conf/ext/editor_p_d_f/Resources/Private/Templates/Vista/";
        $rutaCompletaVista = $directorioVista.'VistaHTML.html';
        $vista = new StandaloneView();
        $vista->setFormat("html");
        $vista->setTemplatePathAndFilename($rutaCompletaVista);

        //Condicion que permite acceder a la generacion de PDF  por medio de codigo HTML
        if ($servicioHTML != "" && ($servicioImagen == "" && $servicioTexto == "")) {

            //Se valida que la opcion para generar pdf de prueba se encuentre vacia
            if (empty($textoHTML)) {
                //Se obtiene el arreglo correspondiente al servicio Json
                $configuracionPDF = $this->recibeJsonServicio($servicioHTML);
                $dompdf = new Dompdf();
                //Se accede al archivo txt que contiene el codigo HTML usado como plantilla (r para abrirlo solo como lectura)
                $archivo = fopen(PATH_site .$configuracionPDF["ubicacionPlantilla"],"r");
                //Se lee el contenido del archivo y se guarda en una variable
                $texto = fread($archivo, filesize(PATH_site .$configuracionPDF["ubicacionPlantilla"]));

                //Se recorre el arreglo que contiene cada una de las marcas y se hace el reemplazo usando las marcas del html
                foreach ($configuracionPDF["marcadoPlantilla"] as $codigo => $valor) {
                    $texto = str_replace($codigo, $valor, $texto);
                }

                //Luego de haber usado lap lantilla txt, el archivo se cierra
                fclose($archivo);

                //Se obtiene el nombre del servidor y la fecha actual para agregarla al archivo PDF generado
                $urlServidor = "http://".$_SERVER["SERVER_NAME"]."/";
                $fechaArchivo = date("d-m-Y_H-i-s");

                //Configuracion del archivo PDF y carga del contenido HTML con las marcas reemplazadas
                $dompdf->setPaper("Letter", "Portrait");
                $dompdf->loadHtml($texto);
                $dompdf ->render();

                //Se guarda el archivo PDF con el nombre y ruta especificada en el servicio
                $archivo_guardado = PATH_site .$configuracionPDF["rutaSalida"].$configuracionPDF["nombreArchivo"]."_".$fechaArchivo.".pdf";
                file_put_contents($archivo_guardado, $dompdf->output());

                //Se genera el enlace de descarga que se envia a la vista
                $ruta_pdf = $urlServidor.$directorio.$configuracionPDF["rutaSalida"].$configuracionPDF["nombreArchivo"]."_".$fechaArchivo.".pdf";
                $enlace_pdf = "<a href='".$ruta_pdf."'>Descargar PDF</a>";

                $vista->assign("link_descarga",$enlace_pdf);
                $vista->assign("ruta_pdf",$ruta_pdf);

            } else {

                 //Codigo similar que genera un PDF utilizando condigo html ingresado en la interfaz backend

                $urlServidor = "http://".$_SERVER["SERVER_NAME"]."/";
                $fechaArchivo = date("d-m-Y_H-i-s");

                if ($textoHTML != null || $textoHTML != "") {
                    $dompdf = new Dompdf();
                    $html = $textoHTML;
                    $dompdf->loadHtml($html);
                    $dompdf->setPaper("Letter", "Portrait");
                    $dompdf->render();

                    if (!is_dir(PATH_site.$rutaSalida)) {
                        mkdir(PATH_site.$rutaSalida, 0777, true);
                    }

                    $archivo_guardado = PATH_site .$rutaSalida.$nombreArchivo."_".$fechaArchivo.".pdf";

                    file_put_contents($archivo_guardado, $dompdf->output());

                    $ruta_pdf = $urlServidor.$directorio.$rutaSalida.$nombreArchivo."_".$fechaArchivo.".pdf";
                    $enlace_pdf = "<a href='".$ruta_pdf."'>Descargar PDF</a>";

                    $vista->assign("link_descarga",$enlace_pdf);
                    $vista->assign("ruta_pdf",$ruta_pdf);
                }
                $vista->assign("html_pdf","<h1>HTML a PDF</h1>");
            }

            //Condicion que permite la edicion de PDF agregando una o mas imagenes en las paginas especificadas
        } else if ($servicioImagen != "" && ($servicioHTML == "" && $servicioTexto == "")) {
            //Se obtiene el arreglo con la configuracion del archivo, imagenes y coordenas que debe ser agregadas
            $arregloImagenes = $this->recibeJsonServicio($servicioImagen);

            //Se obtiene la ubicacion del archivo pdf que se desea editar usando la informacion que entrega el servicio
            $archivoPDF = PATH_site.$arregloImagenes["ubicacionPDFeditable"];
            $pdf = new \FPDI();
            //Se obtiene el numero total de paginas del pdf editable
            $numeroPaginas = $pdf->setSourceFile($archivoPDF);

            //Se recorren todas la paginas del PDF buscando cuales deben ser intervenidas
            for($pagina = 1; $pagina <= $numeroPaginas; $pagina ++) {
                //Se crea el nuevo pdf editado agregando cada una de la paginas originales
                $pdf->AddPage();
                $tplidx = $pdf->importPage($pagina);
                $pdf->useTemplate($tplidx,null,null,0,0,true);

                //Se recorre el arreglo obteneido del servicio localizando en cuales paginas deben agregarse las imagenes
                foreach($arregloImagenes["parametrosImagenes"] as $parametrosImg) {
                    if(isset($parametrosImg["urlImagen"]) && $parametrosImg["urlImagen"] != "") {
                        //Si el numero de pagina actual del documento coincide con una de las pagina espcificadas en el servicio, ls imagen se agrega
                        if($pagina == $parametrosImg["numPagina"]) {
                            $pdf->Image(
                                $parametrosImg["urlImagen"],
                                $parametrosImg["imagenEjeX"],
                                $parametrosImg["imagenEjeY"],
                                $parametrosImg["anchoImagen"],
                                $parametrosImg["altoImagen"]);
                        }
                    }
                }
            }

            $urlServidor = "http://".$_SERVER["SERVER_NAME"]."/";

            //Ruta y nombre del archivo creado
            $fechaArchivo = date("d-m-Y_H-i-s");
            $nombreArchivo = $arregloImagenes["nombreArchivo"]."_".$fechaArchivo;

            $rutaArchivoCreacionPDF = PATH_site.$arregloImagenes["rutaSalida"].$nombreArchivo.".pdf";

            //Ruta del enlace de descarga
            $ruta_pdf = $urlServidor.$directorio.$arregloImagenes["rutaSalida"].$nombreArchivo.".pdf";
            $enlace_pdf = "<a href='".$ruta_pdf."'>Descargar PDF</a>";
            $pdf->Output($rutaArchivoCreacionPDF,"F");
            $vista->assign("link_descarga",$enlace_pdf);
            $vista->assign("ruta_pdf",$ruta_pdf);

            //Condicion que permite la edicion de PDF agregando uno o mas textos en las paginas especificadas
        } else if ($servicioTexto != "" && ($servicioHTML == "" && $servicioImagen == "")) {
            $arregloTextos = $this->recibeJsonServicio($servicioTexto);
            $archivoPDF = PATH_site.$arregloTextos["ubicacionPDFeditable"];

            $pdf = new \FPDI();
            $numeroPaginas = $pdf->setSourceFile($archivoPDF);

            for($pagina = 1; $pagina <= $numeroPaginas; $pagina ++) {
                $pdf->AddPage();
                $tplidx = $pdf->importPage($pagina);
                $pdf->useTemplate($tplidx,null,null,0,0,true);

                foreach($arregloTextos["parametrosTextos"] as $parametrosTxt) {
                    if($pagina == $parametrosTxt["numPagina"]) {
                        $nuevoTexto = iconv('UTF-8','windows-1252',$parametrosTxt["texto"]);

                        $pdf->SetTextColor($parametrosTxt["color"]);
                        $pdf->SetFont($parametrosTxt["fuente"],$parametrosTxt["estilo"],$parametrosTxt["tamano"]);
                        $pdf->SetXY($parametrosTxt["posicionX"],$parametrosTxt["posicionY"]);
                        $pdf->Write(0,$nuevoTexto);
                    }
                }
            }

            $urlServidor = "http://".$_SERVER["SERVER_NAME"]."/";

            //Ruta y nombre del archivo creado
            $fechaArchivo = date("d-m-Y_H-i-s");
            $nombreArchivo = $arregloTextos["nombreArchivo"]."_".$fechaArchivo;
            $rutaArchivoCreacionPDF = PATH_site.$arregloTextos["rutaSalida"].$nombreArchivo.".pdf";

            //Ruta del enlace de descarga
            $ruta_pdf = $urlServidor.$directorio.$arregloTextos["rutaSalida"].$nombreArchivo.".pdf";
            $enlace_pdf = "<a href='".$ruta_pdf."'>Descargar PDF</a>";
            $pdf->Output($rutaArchivoCreacionPDF,"F");
            $vista->assign("link_descarga",$enlace_pdf);
            $vista->assign("ruta_pdf",$ruta_pdf);

            error_log("Link de descarga: ".$ruta_pdf);

        } else {
            //Si no se seleccion una sola opcion o se selecciona mas de una, el programa no hace nada
            $vista->assign("link_descarga","<h4>Verifique que se ingreso solo una de las 3 opciones</h4>");
        }
        return $vista->render();**/
    }

    /**
     * @param string $url
     * @return array
     */
    public function recibeJsonServicio($url) {
        $json = file_get_contents($url);
        $contenido = json_decode($json,true);
        $arregloConfGrafico = $contenido;

        return $arregloConfGrafico;
    }


}
