<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}


\FluidTYPO3\Flux\Core::registerProviderExtensionKey('Francisco.EditorPDF', 'Content');

$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['generacionPDF'] = 'EXT:editor_p_d_f/Classes/EID/servicioHTML.php';
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['edicionPDF_imagen'] = 'EXT:editor_p_d_f/Classes/EID/servicioImagen.php';
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['edicionPDF_texto'] = 'EXT:editor_p_d_f/Classes/EID/servicioTexto.php';

