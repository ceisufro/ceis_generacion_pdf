<html>
<head>
    <meta http-equiv=Content-Type content="text/html; charset=utf-8">
    <meta name=Generator content="Microsoft Word 15 (filtered)">
    <style>
        <!--
        /* Font Definitions */
        @font-face
        {font-family:"Cambria Math";
            panose-1:2 4 5 3 5 4 6 3 2 4;}
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal
        {margin:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            font-size:11.0pt;
            font-family:"Arial",sans-serif;
            color:black;}
        h1
        {margin-top:20.0pt;
            margin-right:0cm;
            margin-bottom:6.0pt;
            margin-left:0cm;
            line-height:115%;
            page-break-after:avoid;
            font-size:20.0pt;
            font-family:"Arial",sans-serif;
            color:black;
            font-weight:normal;}
        h1.CxSpFirst
        {margin-top:20.0pt;
            margin-right:0cm;
            margin-bottom:0cm;
            margin-left:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:20.0pt;
            font-family:"Arial",sans-serif;
            color:black;
            font-weight:normal;}
        h1.CxSpMiddle
        {margin:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:20.0pt;
            font-family:"Arial",sans-serif;
            color:black;
            font-weight:normal;}
        h1.CxSpLast
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:6.0pt;
            margin-left:0cm;
            line-height:115%;
            page-break-after:avoid;
            font-size:20.0pt;
            font-family:"Arial",sans-serif;
            color:black;
            font-weight:normal;}
        h2
        {margin-top:18.0pt;
            margin-right:0cm;
            margin-bottom:6.0pt;
            margin-left:0cm;
            line-height:115%;
            page-break-after:avoid;
            font-size:16.0pt;
            font-family:"Arial",sans-serif;
            color:black;
            font-weight:normal;}
        h2.CxSpFirst
        {margin-top:18.0pt;
            margin-right:0cm;
            margin-bottom:0cm;
            margin-left:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:16.0pt;
            font-family:"Arial",sans-serif;
            color:black;
            font-weight:normal;}
        h2.CxSpMiddle
        {margin:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:16.0pt;
            font-family:"Arial",sans-serif;
            color:black;
            font-weight:normal;}
        h2.CxSpLast
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:6.0pt;
            margin-left:0cm;
            line-height:115%;
            page-break-after:avoid;
            font-size:16.0pt;
            font-family:"Arial",sans-serif;
            color:black;
            font-weight:normal;}
        h3
        {margin-top:16.0pt;
            margin-right:0cm;
            margin-bottom:4.0pt;
            margin-left:0cm;
            line-height:115%;
            page-break-after:avoid;
            font-size:14.0pt;
            font-family:"Arial",sans-serif;
            color:#434343;
            font-weight:normal;}
        h3.CxSpFirst
        {margin-top:16.0pt;
            margin-right:0cm;
            margin-bottom:0cm;
            margin-left:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:14.0pt;
            font-family:"Arial",sans-serif;
            color:#434343;
            font-weight:normal;}
        h3.CxSpMiddle
        {margin:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:14.0pt;
            font-family:"Arial",sans-serif;
            color:#434343;
            font-weight:normal;}
        h3.CxSpLast
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:4.0pt;
            margin-left:0cm;
            line-height:115%;
            page-break-after:avoid;
            font-size:14.0pt;
            font-family:"Arial",sans-serif;
            color:#434343;
            font-weight:normal;}
        h4
        {margin-top:14.0pt;
            margin-right:0cm;
            margin-bottom:4.0pt;
            margin-left:0cm;
            line-height:115%;
            page-break-after:avoid;
            font-size:12.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;
            font-weight:normal;}
        h4.CxSpFirst
        {margin-top:14.0pt;
            margin-right:0cm;
            margin-bottom:0cm;
            margin-left:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:12.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;
            font-weight:normal;}
        h4.CxSpMiddle
        {margin:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:12.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;
            font-weight:normal;}
        h4.CxSpLast
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:4.0pt;
            margin-left:0cm;
            line-height:115%;
            page-break-after:avoid;
            font-size:12.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;
            font-weight:normal;}
        h5
        {margin-top:12.0pt;
            margin-right:0cm;
            margin-bottom:4.0pt;
            margin-left:0cm;
            line-height:115%;
            page-break-after:avoid;
            font-size:11.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;
            font-weight:normal;}
        h5.CxSpFirst
        {margin-top:12.0pt;
            margin-right:0cm;
            margin-bottom:0cm;
            margin-left:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:11.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;
            font-weight:normal;}
        h5.CxSpMiddle
        {margin:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:11.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;
            font-weight:normal;}
        h5.CxSpLast
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:4.0pt;
            margin-left:0cm;
            line-height:115%;
            page-break-after:avoid;
            font-size:11.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;
            font-weight:normal;}
        h6
        {margin-top:12.0pt;
            margin-right:0cm;
            margin-bottom:4.0pt;
            margin-left:0cm;
            line-height:115%;
            page-break-after:avoid;
            font-size:11.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;
            font-weight:normal;
            font-style:italic;}
        h6.CxSpFirst
        {margin-top:12.0pt;
            margin-right:0cm;
            margin-bottom:0cm;
            margin-left:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:11.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;
            font-weight:normal;
            font-style:italic;}
        h6.CxSpMiddle
        {margin:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:11.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;
            font-weight:normal;
            font-style:italic;}
        h6.CxSpLast
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:4.0pt;
            margin-left:0cm;
            line-height:115%;
            page-break-after:avoid;
            font-size:11.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;
            font-weight:normal;
            font-style:italic;}
        p.MsoTitle, li.MsoTitle, div.MsoTitle
        {margin:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:10.0pt;
            font-family:"Arial",sans-serif;
            color:black;
            font-weight:bold;
            font-style:italic;}
        p.MsoTitleCxSpFirst, li.MsoTitleCxSpFirst, div.MsoTitleCxSpFirst
        {margin:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:10.0pt;
            font-family:"Arial",sans-serif;
            color:black;
            font-weight:bold;
            font-style:italic;}
        p.MsoTitleCxSpMiddle, li.MsoTitleCxSpMiddle, div.MsoTitleCxSpMiddle
        {margin:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:10.0pt;
            font-family:"Arial",sans-serif;
            color:black;
            font-weight:bold;
            font-style:italic;}
        p.MsoTitleCxSpLast, li.MsoTitleCxSpLast, div.MsoTitleCxSpLast
        {margin:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:10.0pt;
            font-family:"Arial",sans-serif;
            color:black;
            font-weight:bold;
            font-style:italic;}
        p.MsoSubtitle, li.MsoSubtitle, div.MsoSubtitle
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:16.0pt;
            margin-left:0cm;
            line-height:115%;
            page-break-after:avoid;
            font-size:15.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;}
        p.MsoSubtitleCxSpFirst, li.MsoSubtitleCxSpFirst, div.MsoSubtitleCxSpFirst
        {margin:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:15.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;}
        p.MsoSubtitleCxSpMiddle, li.MsoSubtitleCxSpMiddle, div.MsoSubtitleCxSpMiddle
        {margin:0cm;
            margin-bottom:.0001pt;
            line-height:115%;
            page-break-after:avoid;
            font-size:15.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;}
        p.MsoSubtitleCxSpLast, li.MsoSubtitleCxSpLast, div.MsoSubtitleCxSpLast
        {margin-top:0cm;
            margin-right:0cm;
            margin-bottom:16.0pt;
            margin-left:0cm;
            line-height:115%;
            page-break-after:avoid;
            font-size:15.0pt;
            font-family:"Arial",sans-serif;
            color:#666666;}
        /* Page Definitions */
        @page WordSection1
        {size:595.45pt 841.7pt;
            margin:72.0pt 72.0pt 72.0pt 72.0pt;}
        div.WordSection1
        {page:WordSection1;}
        -->
    </style>

</head>

<body bgcolor=white lang=ES-CL>

<div class=WordSection1>

    <p class=MsoNormal style='line-height:normal'>&nbsp;</p>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=153 style='margin-left:335.25pt;border-collapse:collapse;border:none'>
        <tr>
            <td width=96 valign=top style='width:72.0pt;border:solid black 1.0pt;background:#F3F3F3;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>N°Reunión</span></b></p>
            </td>
            <td width=57 valign=top style='width:42.75pt;border:solid black 1.0pt;border-left:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal>&nbsp;</p>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=600 style='margin-left:5.0pt;border-collapse:collapse;border:none'>
        <tr style='height:21.0pt'>
            <td width=122 valign=top style='width:91.5pt;border:solid black 1.0pt;background:#F3F3F3;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:21.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>Proyecto</span></b></p>
            </td>
            <td width=478 colspan=5 valign=top style='width:358.5pt;border:solid black 1.0pt;border-left:none;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:21.0pt'>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td width=122 valign=top style='width:91.5pt;border:solid black 1.0pt;border-top:none;background:#F3F3F3;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>Fecha</span></b></p>
            </td>
            <td width=147 valign=top style='width:110.25pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><span style='font-size:10.0pt'>dd-mm-aaaa</span></p>
            </td>
            <td width=98 valign=top style='width:73.5pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;background:#F3F3F3;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>Hora Inicio</span></b></p>
            </td>
            <td width=64 valign=top style='width:48.0pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><span style='font-size:10.0pt'>hh:mm</span></p>
            </td>
            <td width=107 valign=top style='width:80.25pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;background:#F3F3F3;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>Hora Término</span></b></p>
            </td>
            <td width=62 valign=top style='width:46.5pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><span style='font-size:10.0pt'>hh:mm</span></p>
            </td>
        </tr>
        <tr style='height:20.0pt'>
            <td width=122 valign=top style='width:91.5pt;border:solid black 1.0pt;border-top:none;background:#F3F3F3;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:20.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>Jefe de Proyecto</span></b></p>
            </td>
            <td width=478 colspan=5 valign=top style='width:358.5pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:20.0pt'>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
            </td>
        </tr>
        <tr style='height:20.0pt'>
            <td width=600 colspan=6 valign=top style='width:450.0pt;border:solid black 1.0pt;border-top:none;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:20.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><u><span style='font-size:10.0pt'>Participantes</span></u></b><b><span style='font-size:10.0pt'>:</span></b></p>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=584 style='border-collapse:collapse;border:none'>
                    <tr>
                        <td width=278 valign=top style='width:208.5pt;border:solid black 1.0pt;background:#F3F3F3;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                            <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Cargo / Rol</span></b></p>
                        </td>
                        <td width=306 valign=top style='width:229.5pt;border:solid black 1.0pt;border-left:none;background:#F3F3F3;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                            <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span
                                    style='font-size:10.0pt'>Nombre</span></b></p>
                        </td>
                    </tr>

                    <f:for each="{personas}" as="persona" iteration="iterador">
                        <tr>
                            <td width=278 valign=top style='width:208.5pt;border:solid black 1.0pt;border-top:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                            </td>
                            <td width=306 valign=top style='width:229.5pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                            </td>
                        </tr>
                    </f:for>

                    <!--
                    <tr>
                        <td width=278 valign=top style='width:208.5pt;border:solid black 1.0pt;border-top:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                            <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                        </td>
                        <td width=306 valign=top style='width:229.5pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                            <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                        </td>
                    </tr>
                    <tr>
                        <td width=278 valign=top style='width:208.5pt;border:solid black 1.0pt;border-top:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                            <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                        </td>
                        <td width=306 valign=top style='width:229.5pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                            <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                        </td>
                    </tr>
                    <tr>
                        <td width=278 valign=top style='width:208.5pt;border:solid black 1.0pt;border-top:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                            <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                        </td>
                        <td width=306 valign=top style='width:229.5pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                            <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                        </td>
                    </tr>-->
                </table>
                <p class=MsoNormal style='line-height:normal'></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal>&nbsp;</p>

    <p class=MsoNormal><b><i><span style='font-size:10.0pt;line-height:115%'>Temas a tratar en reunión: </span></i></b></p>

    <p class=MsoNormal><i><span style='font-size:10.0pt;line-height:115%'>&lt;Marcar con una X los temas a tratar en la reunión;</span></i></p>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=598
           style='margin-left:5.0pt;border-collapse:collapse;border:none'>
        <tr>
            <td width=29 valign=top style='width:21.75pt;border:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
            </td>
            <td width=131 valign=top style='width:98.25pt;border:solid black 1.0pt;border-left:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><span style='font-size:10.0pt'>Registro de Avance</span></p>
            </td>
            <td width=30 valign=top style='width:22.5pt;border:solid black 1.0pt;border-left:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
            </td>
            <td width=117 valign=top style='width:87.75pt;border:solid black 1.0pt;border-left:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><span style='font-size:10.0pt'>Gestión Cambios</span></p>
            </td>
            <td width=32 valign=top style='width:24.0pt;border:solid black 1.0pt;border-left:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
            </td>
            <td width=115 valign=top style='width:86.25pt;border:solid black 1.0pt;border-left:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><span style='font-size:10.0pt'>Gestión Riesgos</span></p>
            </td>
            <td width=30 valign=top style='width:22.5pt;border:solid black 1.0pt;border-left:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
            </td>
            <td width=114 valign=top style='width:85.5pt;border:solid black 1.0pt;border-left:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><span style='font-size:10.0pt'>Otro:<u>indicar acá</u></span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal>&nbsp;</p>

    <div style='border:none;border-top:solid windowtext 1.0pt;padding:1.0pt 0cm 0cm 0cm;width:608px'>
        <p class=MsoNormal style='border:none;padding:0cm'><span style='font-size:12.0pt;line-height:115%'>Gestión de Avance del Proyecto:</span></p>
    </div>

    <p class=MsoTitle style='line-height:normal'><a name=h.iwzagp45yghd></a>&nbsp;</p>

    <p class=MsoNormal style='line-height:normal'>&nbsp;</p>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=601 style='margin-left:5.0pt;border-collapse:collapse;border:none'>
        <tr style='height:20.0pt'>
            <td width=601 colspan=5 valign=top style='width:450.75pt;border:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:20.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>Actividad:</span></b></p>
            </td>
        </tr>
        <tr style='height:20.0pt'>
            <td width=601 colspan=5 valign=top style='width:450.75pt;border:solid black 1.0pt;border-top:none;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:20.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>Responsable:</span></b></p>
            </td>
        </tr>
        <tr>
            <td width=316 valign=top style='width:237.0pt;border:solid black 1.0pt;border-top:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>Desviación respecto al Plan</span></b></p>
            </td>
            <td width=28 valign=top style='width:21.0pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
            </td>
            <td width=103 valign=top style='width:77.25pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><span style='font-size:10.0pt'>Existe</span></p>
            </td>
            <td width=30 valign=top style='width:22.5pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
            </td>
            <td width=124 valign=top style='width:93.0pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><span style='font-size:10.0pt'>No Existe</span></p>
            </td>
        </tr>
        <tr style='height:20.0pt'>
            <td width=601 colspan=5 valign=top style='width:450.75pt;border:solid black 1.0pt;border-top:none;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:20.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>Observaciones y/o Medidas: </span></b></p>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
            </td>
        </tr>
        <tr style='height:20.0pt'>
            <td width=601 colspan=5 valign=top style='width:450.75pt;border:solid black 1.0pt;border-top:none;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:20.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><u><span style='font-size:10.0pt'>Acuerdos </span></u></b><b><span style='font-size:10.0pt'>:</span></b></p>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                <div align=center>
                    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=586 style='border-collapse:collapse;border:none'>
                        <tr style='height:20.0pt'>
                            <td width=116 style='width:87.0pt;border:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:20.0pt'>
                                <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Detalle</span></b></p>
                            </td>
                            <td width=470 colspan=3 valign=top style='width:352.5pt;border:solid black 1.0pt;border-left:none;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:20.0pt'>
                                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                            </td>
                        </tr>
                        <tr>
                            <td width=116 valign=top style='width:87.0pt;border:solid black 1.0pt;border-top:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                                <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>Responsable(s)</span></b></p>
                            </td>
                            <td width=286 valign=top style='width:214.5pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                            </td>
                            <td width=65 valign=top style='width:48.75pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                                <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>Plazo</span></b></p>
                            </td>
                            <td width=119 valign=top style='width:89.25pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                            </td>
                        </tr>
                    </table>
                </div>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=586 style='border-collapse:collapse;border:none'>
                    <tr style='height:20.0pt'>
                        <td width=116 valign=top style='width:87.0pt;border:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:20.0pt'>
                            <p class=MsoNormal align=center style='text-align:center;line-height:normal'><b><span style='font-size:10.0pt'>Detalle</span></b></p>
                        </td>
                        <td width=470 colspan=3 valign=top style='width:352.5pt;border:solid black 1.0pt;border-left:none;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:20.0pt'>
                            <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                            <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                        </td>
                    </tr>
                    <tr>
                        <td width=116 valign=top style='width:87.0pt;border:solid black 1.0pt;border-top:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                            <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>Responsable(s)</span></b></p>
                        </td>
                        <td width=286 valign=top style='width:214.5pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                            <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                        </td>
                        <td width=65 valign=top style='width:48.75pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                            <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>Plazo</span></b></p>
                        </td>
                        <td width=119 valign=top style='width:89.25pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                            <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                        </td>
                    </tr>
                </table>
                <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>....</span></b></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal><i><span style='font-size:10.0pt;line-height:115%'>&lt;copiar esta tabla por cada actividad revisada&gt;</span></i></p>

    <p class=MsoNormal>&nbsp;</p>

    <p class=MsoTitle><a name=h.v5j8khhq4784></a><span style='font-size:12.0pt;line-height:115%'>Gestión de Riesgos del Proyecto:</span></p>

    <p class=MsoNormal>&nbsp;</p>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=600 style='margin-left:5.0pt;border-collapse:collapse;border:none'>
        <tr style='height:20.0pt'>
            <td width=600 colspan=5 valign=top style='width:450.0pt;border:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:20.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><u><span style='font-size:10.0pt'>Descripción del Riesgo</span></u></b><u><span style='font-size:10.0pt'>:</span></u></p>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td width=264 valign=top style='width:198.0pt;border:solid black 1.0pt;border-top:none;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><span style='font-size:10.0pt'>Identificación del Riesgo:</span></b></p>
            </td>
            <td width=32 valign=top style='width:24.0pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
            </td>
            <td width=137 valign=top style='width:102.75pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><span style='font-size:10.0pt'>Potencial</span></p>
            </td>
            <td width=29 valign=top style='width:21.75pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
            </td>
            <td width=138 valign=top style='width:103.5pt;border-top:none;border-left:none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;padding:5.0pt 5.0pt 5.0pt 5.0pt'>
                <p class=MsoNormal style='line-height:normal'><span style='font-size:10.0pt'>Declarado</span></p>
            </td>
        </tr>
        <tr style='height:20.0pt'>
            <td width=600 colspan=5 valign=top style='width:450.0pt;border:solid black 1.0pt;border-top:none;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:20.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><u><span style='font-size:10.0pt'>Acciones Preventivas/ Correctivas:</span></u></b></p>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
            </td>
        </tr>
        <tr style='height:20.0pt'>
            <td width=600 colspan=5 valign=top style='width:450.0pt;border:solid black 1.0pt;border-top:none;padding:5.0pt 5.0pt 5.0pt 5.0pt;height:20.0pt'>
                <p class=MsoNormal style='line-height:normal'><b><u><span style='font-size:10.0pt'>Observaciones:</span></u></b></p>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
                <p class=MsoNormal style='line-height:normal'>&nbsp;</p>
            </td>
        </tr>
    </table>
    <p class=MsoNormal>&nbsp;</p>
</div>
</body>
</html>
